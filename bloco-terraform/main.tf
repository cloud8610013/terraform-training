terraform {
  required_version = ">= 1.0.0"
  required_providers {
    aws = {
      version = "3.73.0"
      source  = "hashicorp/aws"
    }
  }

}

provider "aws" {
  region = "us-west-2"
  default_tags {
    owner      = "agdias"
    managed-by = "terraform"
  }

}
